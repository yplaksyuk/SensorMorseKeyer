/* Name: main.c
 * Author: Yuri Plaksyuk <yuri.plaksyuk@gmail.com>
 * License: <insert your license reference here>
 */

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/sleep.h>

#define PIN_X1 PINB0
#define PIN_X2 PINB1
#define PIN_A  PINB2
#define PIN_B  PINB3

#define MAX_IDLE   60000 // 4 min at 4ms rate
#define RETRY_IDLE 15000 // 1 min at 4ms rate
#define OFF_DELAY     12 // ~ 0.05 sec at 4ms rate

volatile uint16_t idle = 1;

int main(void)
{
	CLKPR = (1 << CLKPCE);                                    // Enable prescaler change
	CLKPR = (1 << CLKPS0);                                    // Write new prescaler value - 2

	PORTB = DDRB = 0;                                         // All pins in Hi-Z state
	GIMSK = (1 << PCIE);                                      // Enable pin change interrupts

	TCCR0A = 0;                                               // Normal operation
	TCCR0B = (1 << CS00);                                     // Internal clock source/No prescaling
	TIMSK0 = (1 << TOIE0);                                    // Enable timer interrupts

	set_sleep_mode(SLEEP_MODE_IDLE);                          // Set sleep mode - IDLE
	sleep_enable();
	sei();

    while (1) {
		sleep_cpu();
    }

    return 0;
}

ISR(TIM0_OVF_vect, ISR_NAKED)
{
	static uint8_t cycle;
	static uint8_t state;
	static uint8_t delay;

	// unset pins for Pin Change interrupts
	PCMSK = 0;

	if (cycle < 4) {
		// update the state
		if (idle)
			state &= ~(1 << cycle);
		else
			state |= (1 << cycle);
		
		// next cycle
		cycle = (cycle - 1) & 0x03;

		if (cycle & 0x02) {
			// X1 -> X2
			DDRB = (DDRB & ~(1 << PIN_X2)) | (1 << PIN_X1);
			
			if (cycle & 0x01)
				PORTB |= (1 << PIN_X1);
			else
				PORTB &= ~(1 << PIN_X1);
			
			PCMSK = (1 << PIN_X2);                         // Set Pins for Pin Change Interrupts
		}
		else {
			// X2 -> X1
			DDRB = (DDRB & ~(1 << PIN_X1)) | (1 << PIN_X2);

			if (cycle & 0x01)
				PORTB |= (1 << PIN_X2);
			else
				PORTB &= ~(1 << PIN_X2);

			PCMSK = (1 << PIN_X1);                         // Set Pins for Pin Change Interrupts
		}
		
		// control output pins
		if (state == 0b0000) {
			if (delay == 0) {
				// set PIN_A and PIN_B to Hi-Z mode
				DDRB &= ~((1 << PIN_A) | (1 << PIN_B));
			}
			else
				delay--;
		}
		else if ((state & 0b0011) == 0) {
			// set PIN_A to active
			DDRB = (DDRB & ~(1 << PIN_B)) | (1 << PIN_A);
			delay = OFF_DELAY;
		}
		else if ((state & 0b1100) == 0) {
			// set PIN_B to active
			DDRB = (DDRB & ~(1 << PIN_A)) | (1 << PIN_B);
			delay = OFF_DELAY;
		}

		if (idle >= MAX_IDLE) {
			// check if we still connected
			if (PINB & (1 << PIN_A)) {
				// connected - postpone power down
				idle -= RETRY_IDLE;
			}
			else {
				// disconnected - go to power down
				PCMSK = (1 << PIN_A);                      // enable pin-change interrupt on PIN_A
				DDRB &= ~((1 << PIN_X1) | (1 << PIN_X2));  // shutdown pins to reduce current drain

				set_sleep_mode(SLEEP_MODE_PWR_DOWN);
				cycle = 4;
			}
		}
	}
	else {
		// switch to IDLE sleep mode
		set_sleep_mode(SLEEP_MODE_IDLE);
		
		// restore the state
		cycle = state = 0;
		idle = 0;
	}

	// increment cycle
	idle++;
	reti();
}

ISR(PCINT0_vect, ISR_NAKED)
{
	idle = 0;
	reti();
}
